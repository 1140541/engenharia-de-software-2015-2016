/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestaoexposicoes.states;

/**
 *
 * @author Luís Maia
 */
public interface CandidaturaState
{
    
    boolean valida();
    
    boolean isInEmSubmissao();
    boolean isInEmDetecaoConflitos();
    boolean isInEmAtribuicao();
    boolean isInEmAvaliacao();
    
    boolean setStateSubmetida();
    boolean setStateSemConflitos();
    boolean setStateComConflitos();
    boolean setStateEmDetecaoConflitos();
    boolean setStateEmAvaliacao();
    boolean setStateEmAtribuicao();
    boolean setStateEmDistribuicao();
    boolean setStateEmAtribuido();
    boolean setStateEmAvalidado();
     boolean setStateEmAceite();
      boolean setStateEmRejeitada();
      boolean setStateConfirmada();
   
    
}
