package gestaoexposicoes.states;

/**
 *
 * @author 
 */
public interface ExposicaoState {

    /**
     * Valida estado do evento
     *
     * @return resposta validação
     */
    public boolean valida();

    /**
     * Evento no estado registado
     *
     * @return resposta estado
     */
    public boolean emRegistado();

    /**
     * Evento no estado Sessões Temáticas Definidas
     *
     * @return resposta estado
     */
    public boolean emSTDefinidas();

    /**
     * Evento no estado Comissão de Programa Definida
     *
     * @return resposta estado
     */
    public boolean emCPDefinida();

    /**
     * Evento em submissão
     *
     * @return resposta estado
     */
    public boolean emSubmissao();

    /**
     * Evento em deteção de conflitos
     *
     * @return resposta estado
     */
    public boolean emDetecaoConflitos();

    /**
     * Evento em licitação
     *
     * @return resposta estado
     */
    public boolean emAvalia();

    /**
     * Evento em distribuição
     *
     * @return resposta estado
     */
    public boolean emDistribuicao();

    /**
     * Evento em revisão
     *
     * @return resposta estado
     */
    public boolean emRevisao();

    /**
     * Evento em decisão
     *
     * @return resposta estado
     */
    public boolean emDecisao();

    /**
     * Evento no estado decidido
     *
     * @return resposta estado
     */
    public boolean emDecidido();

    /**
     * Evento no estado cameraReady
     *
     * @return resposta estado
     */
    public boolean emCameraReady();

    /**
     * Modifica estado do evento para registado
     *
     * @return resposta estado
     */
    public boolean setEstadoRegistado();

    /**
     * Modifica estado do evento para submissão
     *
     * @return resposta estado
     */
    public boolean setEstadoEmSubmissao();

    /**
     * Modifica estado do evento para deteção de conflitos
     *
     * @return resposta estado
     */
    public boolean setEstadoEmDetecaoConflitos();


    /**
     * Modifica estado do evento para distibuição
     *
     * @return resposta estado
     */
    public boolean setEstadoEmDistribuicao();

    /**
     * Modifica estado do evento para estado avaliacao
     *
     * @return resposta estado
     */
    public boolean setEstadoEmAvaliacao();

    /**
     * Modifica estado do evento para estado decisão
     *
     * @return resposta estado
     */
    public boolean setEstadoEmDecisao();

    /**
     * Modifica estado do evento para estado decidido
     *
     * @return resposta estado
     */
    public boolean setEstadoEmDecidido();


}
