package gestaoexposicoes.states;

import gestaoexposicoes.model.Exposicao;

/**
 *
 * @author Luís Maia
 */
public class ExposicaoRegistadoEstado extends ExposicaoEstadoCriado   {

    public ExposicaoRegistadoEstado(Exposicao e) {
        super(e);
    }

    @Override
    public boolean valida() {
    return true;
    }

    @Override
    public boolean setEstadoRegistado() {
        return true;
    }

    @Override
    public boolean emRegistado() {
        if (setEstadoRegistado()) {
            return true;
        }
        return false;
    }

    
    @Override
    public String toString()
    {
        return "Registado";
    }


    @Override
    public boolean emAvalia() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setEstadoEmAvaliacao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
