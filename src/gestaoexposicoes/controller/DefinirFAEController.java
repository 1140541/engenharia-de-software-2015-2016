/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Exposicao;
import gestaoexposicoes.model.FAE;
import gestaoexposicoes.model.RegistoExposicao;
import gestaoexposicoes.model.RegistoUtilizadores;
import gestaoexposicoes.model.Utilizador;
import java.util.List;

/**
 *
 * @author rosamarianascimentodasilva
 */
public class DefinirFAEController {

    private final RegistoExposicao re;
    private final RegistoUtilizadores ru;
    private Exposicao m_exp;

    private String strId;

    public DefinirFAEController(RegistoExposicao re, RegistoUtilizadores ru) {
        this.re = re;
        this.ru = ru;
    }

    public List<Exposicao> getExposicoesOrganizador(String strId) {
        return re.getExposicaoOrganizador(strId);
    }

    public List<Utilizador> getUtilizadores() {
        return ru.getUtilizadores();
    }

    public void selectExposicao(Exposicao e) {
        this.m_exp = e;
    }

    public FAE addMembroFAE(String strID) {
        Utilizador u = ru.getUtilizador(strID);

        if (u != null) {
            return m_exp.addMembroFAE(u);
        } else {
            return null;
        }
    }

    public boolean registaMembroFAE(FAE f) {
        return m_exp.registaMembroFAE(f);
    }
}
