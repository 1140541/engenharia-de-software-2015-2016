/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Exposicao;
import gestaoexposicoes.model.RegistoExposicao;
import gestaoexposicoes.model.RegistoUtilizadores;
import gestaoexposicoes.model.Utilizador;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public class CriarExposicaoController {

    private final RegistoExposicao re;
    private final RegistoUtilizadores ru;
    private Exposicao m_exposicao;

    public CriarExposicaoController(RegistoExposicao re, RegistoUtilizadores ru) {
        this.re = re;
        this.ru = ru;
    }

    public void novaExposicao() {
        this.m_exposicao = this.re.novaExposicao();
    }

    public void setDados(String sTitulo, String sDescritivo, Date oDtInicio, Date oDtFim, String sLocal) {
        this.m_exposicao.setTitulo(sTitulo);
        this.m_exposicao.setDescritivo(sDescritivo);
        this.m_exposicao.setPeriodo(oDtInicio, oDtFim);
        this.m_exposicao.setLocal(sLocal);
    }

    public List<Utilizador> getListaUtilizadores() {
        return this.ru.getUtilizadores();
    }

    public void addOrganizador(Utilizador u) {
        this.m_exposicao.addOrganizador(u);
    }

    public boolean validaExposicao() {
        return this.re.validaExposicao(m_exposicao);
    }

    public boolean registaExposicao() {
        return this.re.registaExposicao(m_exposicao);
    }

    public Exposicao getExposicao() {
        return m_exposicao;
    }

    public String getExposicaoString() {
        return this.m_exposicao.toString();
    }
}
