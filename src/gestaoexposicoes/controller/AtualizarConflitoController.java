/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.*;
import java.util.Objects;

/**
 *
 * @author Luís Maia
 */
public class AtualizarConflitoController {
    private RegistoConflitos rc;

    public AtualizarConflitoController(CentroExposicoes ce, FAE fae) {
        this.rc = ce.getRegistoConflitos();
    }

    public RegistoConflitos getRc() {
        return rc;
    }

    public void setRc(RegistoConflitos rc) {
        this.rc = rc;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AtualizarConflitoController other = (AtualizarConflitoController) obj;
        if (!Objects.equals(this.rc, other.rc)) {
            return false;
        }
        return true;
    }
    
    
    
}
