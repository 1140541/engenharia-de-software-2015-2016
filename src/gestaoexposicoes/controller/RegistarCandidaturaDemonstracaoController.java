/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.Candidatura;
import gestaoexposicoes.model.CandidaturaDemonstracao;
import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Demonstracao;
import gestaoexposicoes.model.ListaCandidaturas;
import gestaoexposicoes.model.Representante;
import java.util.ArrayList;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class RegistarCandidaturaDemonstracaoController {

    private Candidatura c;
    private CandidaturaDemonstracao cd;
    private CentroExposicoes centro;
    private Demonstracao d;

    public ListaCandidaturas getListaCandidatura(Representante r) {
        return centro.getListaCandidatura(r);
    }

    public ArrayList<Demonstracao> getDemonstracoesDefinidas(Candidatura c) {
        return c.getDemonstracoesDefinidas();
    }

    public void setDados(String nomeEmpresa, String morada, int tlm) {
        cd = new CandidaturaDemonstracao();
        cd.setNomeEmpresa(nomeEmpresa);
        cd.setMorada(morada);
        cd.setTlm(tlm);
        cd.valida();
    }

    public void registaCandidatura() {
        d.addCandidatura(cd);
    }

    /**
     * @param d the d to set
     */
    public void setDemonstracao(Demonstracao d) {
        this.d = d;
    }
}
