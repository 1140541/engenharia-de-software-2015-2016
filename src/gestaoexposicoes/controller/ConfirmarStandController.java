/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.Candidatura;
import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Representante;
import gestaoexposicoes.model.Stand;
import java.util.ArrayList;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class ConfirmarStandController {

    private Candidatura c;
    private Stand s;
    private CentroExposicoes centro;

    public ArrayList<Candidatura> getCandidaturasAceites(Representante r) {
        return centro.getCandidaturasAceites(r);
    }

    /**
     * @param c the c to set
     */
    public void setCandidatura(Candidatura c) {
       s= c.getStand();
    }
    
    
    public void confirmarStand() {
        c.setConfirmada();
        s.setCandidatura();
    }
}
