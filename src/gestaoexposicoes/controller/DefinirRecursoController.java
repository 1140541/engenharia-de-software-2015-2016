/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Recurso;
import gestaoexposicoes.model.RegistoRecursos;

/**
 *
 * @author Luís Maia
 */
public class DefinirRecursoController {
    private RegistoRecursos rr;

    public DefinirRecursoController(CentroExposicoes ce) {
        this.rr = ce.getlRecurso();
    }
    
    public void novoRecurso(String designacao){
        rr.novoRecurso(designacao);
    }
    
    public void addRecurso(Recurso r){
        rr.registaRecurso(r);
    }
    
}
