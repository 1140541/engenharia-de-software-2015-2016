/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.*;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public class CriarDemonstracaoController {
     private final RegistoRecursos rr;
    private RegistoExposicao re;
    private Demonstracao demonstracao;

    public CriarDemonstracaoController(CentroExposicoes ce) {
        rr = ce.getlRecurso();
    }

    public void novaDemonstracao() {
    }

    public void setDados(String codigo, String sDescritivo) {
        this.demonstracao.setCodigo(codigo);
        this.demonstracao.setDescricao(sDescritivo);
    }

    public List<Recurso> getListaRecursos() {
        return this.rr.getRegistoRecurso();
    }

    public void addRecurso(Recurso r) {
        this.demonstracao.addRecurso(r);
    }

    public boolean validaDemonstracao() {
        return this.re.validaDemonstracao(demonstracao);
    }

    public boolean registaDemonstracao() {
        return this.re.registaDemonstracao(demonstracao);
    }

    public Demonstracao getDemonstracao() {
        return this.demonstracao;
    }

    public String getDemonstracaoString() {
        return this.demonstracao.toString();
    }
}
