/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Avaliação;
import gestaoexposicoes.model.Exposicao;
import gestaoexposicoes.model.FAE;
import gestaoexposicoes.model.RegistoExposicao;
import gestaoexposicoes.model.RegistoUtilizadores;
import gestaoexposicoes.model.Utilizador;
import java.util.Date;
import java.util.List;

/**
 *
 * @author cferreira
 */
public class AvaliarCandidaturasController {

    private final RegistoExposicao m_oCE;
    private Exposicao m_exposicao;
    private Avaliação m_decisao;
    private FAE m_oFfae;
    
    public AvaliarCandidaturasController(RegistoExposicao ce) {
        this.m_oCE = ce;
    }

    public List<Exposicao> getListaExposicoesDoFAE(Utilizador u) {
        return this.m_oCE.getListaExposicoesDoFAE(u);
    }

    public List<Avaliação> selecionaExposicao(Exposicao exposicao, Utilizador u) {
        this.m_exposicao=exposicao;
        this.m_oFfae=m_exposicao.getFAE(u);
        
        return this.m_exposicao.getListaCandidaturaPorDecidir(m_oFfae);
    }

    public String getInformacaoDaCandidaturaPorDecidir(Avaliação d) {
        this.m_decisao=d;
        return this.m_exposicao.getInformacaoDaCandidaturaPorDecidir(d);
    }
      
    public void setDecisao(Boolean dec, String textoJustificacao) {
        this.m_decisao.setDecisao(dec);
        this.m_decisao.setTextoDescricao(textoJustificacao);
    }

    public void registaDecisao() {
        this.m_exposicao.registaDecisao(m_decisao);
        
    }

  
    
}


    
   
   