/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestaoexposicoes.ui;

import gestaoexposicoes.model.Avaliação;
import gestaoexposicoes.controller.AvaliarCandidaturasController;
import gestaoexposicoes.model.*;
import gestaoexposicoes.model.Exposicao;
import gestaoexposicoes.model.Utilizador;
import gestaoexposicoes.utils.Utils;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class AvaliarCandidaturasUI
{
    private final AvaliarCandidaturasController m_oController;
    public final Utilizador m_oUu;
    private List<Exposicao> lExposicoes; 
    private List<Avaliação> lDecisoes;
    
    public AvaliarCandidaturasUI(RegistoExposicao re, Utilizador u)
    {
        this.m_oController = new AvaliarCandidaturasController(re);
        this.m_oUu=u;
    }
    
    public void run()
    {
        System.out.println("\nDecidir Candidaturas:");
        
        
        List<Exposicao> lExposicoes = m_oController.getListaExposicoesDoFAE(m_oUu);
        Object objExposicao;
        objExposicao=Utils.apresentaESeleciona(lExposicoes, "Selecione Exposição");
        
        lDecisoes=m_oController.selecionaExposicao((Exposicao) objExposicao,m_oUu);
        Object objDecisao;
        objDecisao=Utils.apresentaESeleciona(lDecisoes, "Selecione Candidatura");
        
        System.out.println("Info da Candidatura:\n" + 
                m_oController.getInformacaoDaCandidaturaPorDecidir((Avaliação) objDecisao));
        
        // Introduz Justificação
        Boolean s_dec;
        String s_textoJustificacao;
        s_dec = Utils.confirma("Aceita(S) ou Rejeita(N) Candidatura?");
        s_textoJustificacao =Utils.readLineFromConsole("introduz Texto Justificativo");
        
        m_oController.setDecisao(s_dec, s_textoJustificacao); 
        
        m_oController.registaDecisao();
         
    }      
}
  
