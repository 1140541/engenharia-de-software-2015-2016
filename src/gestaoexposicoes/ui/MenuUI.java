/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.ui;

import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.RegistoExposicao;
import gestaoexposicoes.model.RegistoRecursos;
import gestaoexposicoes.model.RegistoUtilizadores;
import gestaoexposicoes.model.Utilizador;
import gestaoexposicoes.utils.Utils;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MenuUI {

    private final CentroExposicoes m_ce;
    private final RegistoExposicao re;
    private final RegistoUtilizadores ru;
    private final RegistoRecursos rr;
    private String opcao;

    public MenuUI(CentroExposicoes m_ce, RegistoExposicao re, RegistoUtilizadores ru, RegistoRecursos rr) {
        this.m_ce = m_ce;
        this.re = re;
        this.ru = ru;
        this.rr = rr;
    }

    public void run() throws IOException {
        //Uma espécie de Login
        List<Utilizador> lUsers = this.m_ce.getRegistoUtilizadores();
        Utilizador utilizador;
        do {
            utilizador = (Utilizador) Utils.apresentaESeleciona(lUsers, "Selecione Utilizador");

        } while (utilizador == null);

        do {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Criar Exposição");
            System.out.println("2. Definir FAE");
            System.out.println("3. Atribuir Candidatura");
            System.out.println("4. Decidir Candidaturas");
            System.out.println("5. Registar Candidatura");
            System.out.println("6. Registar Utilizador");
            System.out.println("7. Confirmar Registo de Utilizador");

            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            switch (opcao) {
                case "1":
                    CriarExposicaoUI ui1 = new CriarExposicaoUI(ru, re, (Utilizador) utilizador);
                    ui1.run();
                    break;
                case "2":
                    DefinirFAEUI ui2 = new DefinirFAEUI(m_ce, ru, re);
                    ui2.run();
                    break;
                case "4":
                    AvaliarCandidaturasUI ui4 = new AvaliarCandidaturasUI(re, utilizador);
                    ui4.run();
                    break;
                case "6":
                    RegistarUtilizadorUI ui6 = new RegistarUtilizadorUI(m_ce, utilizador);
                    ui6.run();
                    break;
                case "7":
                    ConfirmarRegistoUtilizadorUI ui7 = new ConfirmarRegistoUtilizadorUI(ru, utilizador);
                    ui7.run();
                    break;
            }
        } while (!opcao.equals("0"));
    }
}
