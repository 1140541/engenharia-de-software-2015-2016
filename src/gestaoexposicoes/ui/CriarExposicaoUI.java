/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.ui;

import gestaoexposicoes.controller.CriarExposicaoController;
import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.RegistoExposicao;
import gestaoexposicoes.model.RegistoUtilizadores;
import gestaoexposicoes.model.Utilizador;
import gestaoexposicoes.utils.Utils;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class CriarExposicaoUI {

    private final CriarExposicaoController m_oController;
    private Utilizador m_oUu;

    public CriarExposicaoUI(RegistoUtilizadores ru, RegistoExposicao re, Utilizador u) {
        this.m_oController = new CriarExposicaoController(re, ru);
        m_oUu = u;
    }

    public void run() {
        System.out.println("\nCriar Exposicao:");
        m_oController.novaExposicao();

        introduzDados();

        do {
            System.out.println("Tem de adicionar pelo menos dois organizadores.");
            adicionaOrganizadores();
        } while (m_oController.getExposicao().getM_lOrganizadores().size() < 2);

        apresentaDados();

        if (Utils.confirma("Confirma os dados da exposição? (S/N)")) {
            if (m_oController.validaExposicao()) {
                if (m_oController.registaExposicao()) {
                    System.out.println("Exposição criada com sucesso.");
                }
            } else {
                System.out.println("Não foi possivel guardar corretamente a Exposição.");
            }
        } else {
            System.out.println("Exposição não guardada.");
        }
    }

    private void introduzDados() {
        String sTitulo = Utils.readLineFromConsole("Introduza Título: ");
        String sDescritivo = Utils.readLineFromConsole("Introduza Descritivo: ");
        Date oDtInicio = Utils.readDateFromConsole("Introduza Data Inicio (dd-mm-aaaa): ");
        Date oDtFim = Utils.readDateFromConsole("Introduza Data Fim (dd-mm-aaaa): ");
        String sLocal = Utils.readLineFromConsole("Introduza Local: ");

        m_oController.setDados(sTitulo, sDescritivo, oDtInicio, oDtFim, sLocal);
    }

    private void apresentaDados() {
        System.out.println("\nExposicao:\n" + m_oController.getExposicaoString());
    }

    private void adicionaOrganizadores() {
        List<Utilizador> lUsers = m_oController.getListaUtilizadores();
        Object obj;
        do {
            obj = Utils.apresentaESeleciona(lUsers, "Selecione Utilizador -> Organizador");
            if (obj != null) {
                m_oController.addOrganizador((Utilizador) obj);
            }
        } while (obj != null);

    }
}
