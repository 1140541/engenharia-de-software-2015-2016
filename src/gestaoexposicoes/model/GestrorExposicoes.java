/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.Objects;

/**
 *
 * @author Luís Maia
 */
public class GestrorExposicoes {

    private Utilizador utilizador;

    public GestrorExposicoes() {
    }

    public Utilizador getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    @Override
    public String toString() {
        return "GestrorExposicoes{" + "utilizador=" + utilizador + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GestrorExposicoes other = (GestrorExposicoes) obj;
        if (!Objects.equals(this.utilizador, other.utilizador)) {
            return false;
        }
        return true;
    }

}
