/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.Objects;

/**
 *
 * @author Luís Maia
 */
public class Local {

    private String morada;

    public Local(String morada) {
        this.morada = morada;
    }

    public Local() {
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Local other = (Local) obj;
        if (!Objects.equals(this.morada, other.morada)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Local{" + "morada=" + morada + '}';
    }

}
