/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

/**
 *
 * @author Luís Maia
 */
public class Conflito {

    private TipoConflito tc;
    private Candidatura c;
    private FAE fae;

    public Conflito(TipoConflito tc, Candidatura c, FAE fae) {
        this.tc = tc;
        this.c = c;
        this.fae = fae;
    }

    public Conflito(TipoConflito tc) {
        this.tc = tc;
    }

    public TipoConflito getTc() {
        return tc;
    }

    public void setTc(TipoConflito tc) {
        this.tc = tc;
    }

    public Candidatura getC() {
        return c;
    }

    public void setC(Candidatura c) {
        this.c = c;
    }

    public FAE getFae() {
        return fae;
    }

    public void setFae(FAE fae) {
        this.fae = fae;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conflito other = (Conflito) obj;
        return true;
    }

}
