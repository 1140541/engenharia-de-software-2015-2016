package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public interface MecanismoAtribuicao {

    public boolean atribui(Criterio c);
    
    public String getInfo();
}
