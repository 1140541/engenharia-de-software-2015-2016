/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Luís Maia
 */
public class RegistoTipoConflito {
    private List<TipoConflito> ltp;

    public RegistoTipoConflito(TipoConflito ltp) {
        this.ltp.add(ltp);
    }

    public List<TipoConflito> getLtp() {
        return ltp;
    }

    public void setLtp(List<TipoConflito> ltp) {
        this.ltp = ltp;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistoTipoConflito other = (RegistoTipoConflito) obj;
        if (!Objects.equals(this.ltp, other.ltp)) {
            return false;
        }
        return true;
    }

}
