/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public class RegistoExposicao {

    private final List<Exposicao> m_lExposicoes;
    private RegistoUtilizadores registoUtilizadores;

    public RegistoExposicao() {
        this.m_lExposicoes = new ArrayList<>();
    }

    public Exposicao novaExposicao() {
        return new Exposicao();
    }

    public List<Exposicao> getM_lExposicoes() {
        return m_lExposicoes;
    }

    public boolean validaExposicao(Exposicao e) {
        if (e.valida()) {
            // Introduzir as validações aqui
            return true;
        }
        return false;
    }

    public boolean registaExposicao(Exposicao e) {
        return this.addExposicao(e);
    }

    private boolean addExposicao(Exposicao e) {
        return this.m_lExposicoes.add(e);
    }

    public List<Exposicao> getListaExposicoesDoFAE(Utilizador u) {
        List<Exposicao> l_ExpDoFAE = new ArrayList();

        for (Exposicao m : this.m_lExposicoes) {
            if (m.getFAE(u) != null) {
                l_ExpDoFAE.add(m);
            }

        }
        return l_ExpDoFAE;
    }

    public List<Exposicao> getExposicaoOrganizador(String strId) {
        List<Exposicao> leOrganizador = new ArrayList<Exposicao>();

        Utilizador u = registoUtilizadores.getUtilizador(strId);

        if (u != null) {
            for (Iterator<Exposicao> it = this.m_lExposicoes.listIterator(); it.hasNext();) {
                Exposicao e = it.next();

                if (e.hasOrganizador(u)) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    public boolean validaDemonstracao(Demonstracao demonstracao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean registaDemonstracao(Demonstracao demonstracao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
