/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Luís Maia
 */
public class Candidatura {
    private String codigo;
    private String descrição;
    private List<Recurso> lstRecursos;

    public Candidatura(String codigo, String descrição, List<Recurso> lstRecursos) {
        this.codigo = codigo;
        this.descrição = descrição;
        this.lstRecursos = lstRecursos;
    }
    
    public boolean validaCodigo(){
        return  true;
    }
    
    public boolean validaRecurso(Recurso r) {
        return true;
    }
    
    public void addRecurso(Recurso r){
        lstRecursos.add(r);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Candidatura other = (Candidatura) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    public List<Recurso> getLstRecursos() {
        return lstRecursos;
    }

    public void setLstRecursos(List<Recurso> lstRecursos) {
        this.lstRecursos = lstRecursos;
    }

    public void setDecisao(boolean d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setStand(Stand s) {
  //     setAceite();  
    }

    public void setRejeitada() {
   //setRejeitada()
    }

    public Stand getStand() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setConfirmada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<Demonstracao> getDemonstracoesDefinidas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
